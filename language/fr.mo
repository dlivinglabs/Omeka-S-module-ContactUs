��    7      �  I   �      �     �  .   �     �  B        G     \  
   q     |     �  #   �  (   �  K   �     ?  �   ^  $   �  $     $   :  &   _  &   �  &   �  $   �  $   �  $     %   C  R   i      �  $   �  k   	     n	  "   �	     �	     �	     �	  f   �	  t   J
     �
     �
  F   �
  J   -  6   x  !   �     �  ^   �  U   D  7   �  [   �  S   .  W   �     �  3   �  "   !  %   D  M   j     �  j  �  "   0  4   S     �  j   �               .     =     \  /   o  1   �  Y   �     +  �   <  $   �  $     $   2  '   W  '     '   �  &   �  &   �  &     '   D  X   l  2   �  4   �  �   -  3   �  &   �     $     D     R  k   Z  �   �  "   Y     |  a   �  q   �  N   c      �     �  i   �  b   Q  A   �  u   �  t   l  `   �     B  @   _  $   �  "   �  _   �     H     '           1   *       7   -         +         $   ,          &                             6              !   #                  4             (   %      /      0       
                          	   5   "   )                 3       2   .                    About resource %s (%s). Append a form to allow visitors to contact us. Block title Check emails for notifications or remove them to use default ones. Confirmation contact Confirmation message Contact us Default emails to notify Emails to notify Enable simple antispam for visitors Error when sending email. Arguments:\n%s First email is used for confirmation.
contact@example.org
info@example2.org Heading for the block, if any. Hi {name},

Thanks to contact us!

We will answer you soon.

Sincerely,

{main_title}
{main_url}

--

Your message:
Subject: {subject}

{message} How many are one plus 1 (in number)? How many are one plus 2 (in number)? How many are one plus 3 (in number)? How many are three plus 1 (in number)? How many are three plus 2 (in number)? How many are three plus 3 (in number)? How many are two plus 1 (in number)? How many are two plus 2 (in number)? How many are two plus 3 (in number)? How many are zero plus 1 (in number)? How many are zero plus 1 (in number)? = 1
How many are one plus 1 (in number)? = 2 Leave empty to use site settings Leave empty to use the default value Let empty to use main settings. First email is used for confirmation.
contact@example.org
info@example2.org Let empty to use site settings. List of antispam questions/answers List of recipients to notify Mail subject Message Possible placeholders: {main_title}, {main_url}, {site_title}, {site_url}, {email}, {name}, {message}. See the block "Contact us" for a simple list. Separate questions and answer with a "=". Questions may be translated. Send a confirmation email Send message Separate questions and answer with a "=". Questions may be translated. Sorry, we are not able to send email to notify the admin. Come back later. Sorry, we are not able to send the confirmation email. Subject of the confirmation email Template to display Templates are in folder "common/block-layout" of the theme and should start with "contact-us". Thank you for your message %s. Check your confirmation mail. We will answer you soon. Thank you for your message %s. We will answer you soon. The default list of recipients to notify, one by row. First email is used for confirmation. The list of recipients to notify, one by row. First email is used for confirmation. The module "%s" was automatically deactivated because the dependencies are unavailable. There is an error. This module has resources that connot be installed. This module requires modules "%s". This module requires the module "%s". To create antispam, each question must be separated from the answer by a "=". [Contact] %s Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-26 00:00+0000
Last-Translator: Daniel Berthereau <Daniel.fr@Berthereau.net>
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.4.1
 À propos de la ressource %s (%s). Ajouter un formulaire de contact pour les visiteurs. Titre du bloc Vérifiez les emails destinés aux notifications, ou supprimez les pour utiliser les adresses par défaut. Confirmation contact Message de confirmation Contactez-nous Emails à notifier par défaut Emails à informer Activer l’anti-spam simple pour les visiteurs Erreur lors de l’envoi du mail. Arguments :\n%s Le premier email est utilisé pour la confirmation.
contact@example.org
info@example2.org Entête du bloc. Bonjour {name},

Merci de nous contacter !

Nous vous répondrons prochainement.

Cordialement,

{main_title}
{main_url}

--

Votre message:
Sujet : {subject}

{message} Combien font un et 1 (en chiffres) ? Combien font un et 2 (en chiffres) ? Combien font un et 3 (en chiffres) ? Combien font trois et 1 (en chiffres) ? Combien font trois et 2 (en chiffres) ? Combien font trois et 3 (en chiffres) ? Combien font deux et 1 (en chiffres) ? Combien font deux et 2 (en chiffres) ? Combien font deux et 3 (en chiffres) ? Combien font zéro et 1 (en chiffres) ? Combien font zéro plus 1 (en nombre) ? = 1
Combien font un plus 1 (en nombre) ? = 2 Laisser vide pour utiliser les paramètres du site Laisser vide pour utiliser le paramètre par défaut Laisser vide pour utiliser les paramètres généraux. Le premier email est utilisé pour le message de confirmation.
contact@example.org
info@example2.org Laisser vide pour utiliser les paramètres du site. Liste de questions/réponses anti-spam Liste des destinataires du mail Sujet du mail Message Les jokers possibles sont : {main_title}, {main_url}, {site_title}, {site_url}, {email}, {name}, {message}. Voir le bloc "Contact us" pour une simple liste. Séparer les questions et les réponses avec un signe "=". Les questions peuvent être traduites. Envoyer un message de confirmation Envoyer le message Séparer les questions et les réponses avec un signe "=". Les questions peuvent être traduites. Désolé, nous ne pouvons pas envoyer un mail pour informer l’administrateur. Merci de revenir ultérieurement. Désolé, nous ne sommes pas en mesure d’envoyer le message de confirmation. Sujet du message de confirmation Gabarit à afficher Les gabarits sont dans le dossier "common/block-layout" du thème et doivent commencer avec "contact-us". Merci pour votre message %s. Vérifiez le message de confirmation. Nous répondrons prochainement. Merci pour votre message %s. Nous vous répondrons prochainement. La liste des destinataires par défaut, une par ligne. Le premier email est utilisé pour le message de confirmation. La liste des destinataires par défaut, un par ligne. Le premier email est utilisé pour le message de confirmation. Le module %s" a été automatiquement désactivé car ses dépendances ne sont plus disponibles. Une erreur s’est produite. Ce module a des ressources qui ne peuvent pas être installées. Ce module requiert les modules "%s". Ce module requiert le module "%s". Pour créer un anti-spam, chaque question doit être séparé de la réponse avec un signe "=". [Contact] %s 